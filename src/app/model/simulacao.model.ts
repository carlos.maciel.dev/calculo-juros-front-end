export interface SimulacaoModel {
  id: number;
  quantidadeParcelas: number;
  valorJuros: number;
  valorParcela: number;
  valorSolicitado: number;
  valorTotalJuros: number;
}
