import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { ClienteComponent } from './view/cliente/cliente.component';
import {MatDialogModule} from '@angular/material/dialog';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatSidenavModule} from '@angular/material/sidenav';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import { HttpClientModule } from '@angular/common/http';
import { CreateEditClienteComponent } from './view/cliente/create-edit-cliente/create-edit-cliente.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { ExcluirDialogComponent } from './shared/excluir-dialog/excluir-dialog.component';
import { SimularEmprestimoComponent } from './shared/simular-emprestimo/simular-emprestimo.component';
import {MatTableModule} from '@angular/material/table';
import { HistoricoSimulacaoComponent } from './shared/historico-simulacao/historico-simulacao.component';
import { SobreComponent } from './shared/sobre/sobre.component';



@NgModule({
  declarations: [
    AppComponent,
    ClienteComponent,
    CreateEditClienteComponent,
    ExcluirDialogComponent,
    SimularEmprestimoComponent,
    HistoricoSimulacaoComponent,
    SobreComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatGridListModule,
    MatDialogModule,
    MatCardModule,
    MatToolbarModule,
    MatSidenavModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatListModule,
    MatDividerModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatTableModule

  ],
  entryComponents: [
    ExcluirDialogComponent,
    SimularEmprestimoComponent,
    HistoricoSimulacaoComponent,
    SobreComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
