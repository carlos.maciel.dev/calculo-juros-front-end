import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClienteService } from 'src/app/services/cliente-service.service';
import { Cliente } from 'src/app/model/cliente.model';
import { ActivatedRoute, Router } from '@angular/router';
import { async } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-create-edit-cliente',
  templateUrl: './create-edit-cliente.component.html',
  styleUrls: ['./create-edit-cliente.component.css']
})
export class CreateEditClienteComponent implements OnInit {
  form: FormGroup;
  cliente: Cliente;
  selected: any;
  mode: any;
  bairro: any;
  cep: any;
  nome: any;
  numero: any;
  renda: any;
  rua: any;
  risco:any;
  idCliente: any;
  constructor(private clienteService: ClienteService,  private route: ActivatedRoute, private router:Router, private snackbar:MatSnackBar) { }

  ngOnInit() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.idCliente = id;

    if(id.toString() == 'NaN'){

      this.mode = 'cadastrar';
      this.form = new FormGroup({
        bairro: new FormControl( '', [Validators.required]),
        cep: new FormControl(    '', [Validators.required]),
        nome: new FormControl(   '', [Validators.required]),
        numero: new FormControl( '', [Validators.required]),
        renda: new FormControl(  '', [Validators.required]),
        rua: new FormControl(    '', [Validators.required]),

      });

    } else {

      this.form = new FormGroup({
        bairro: new FormControl(this.bairro, [Validators.required]),
        cep: new FormControl(this.cep, [Validators.required]),
        nome: new FormControl(this.nome, [Validators.required]),
        numero: new FormControl(this.numero, [Validators.required]),
        renda: new FormControl(this.renda, [Validators.required]),
        rua: new FormControl(this.rua, [Validators.required]),

      });


      const id = +this.route.snapshot.paramMap.get('id');
      this.clienteService.getId(id).subscribe (cliente =>{
      this.bairro = cliente.bairro;
      this.cep = cliente.cep;
      this.nome = cliente.nome;
      this.numero = cliente.numero;
      this.renda = cliente.renda;
      this.rua = cliente.rua;
      this.risco = cliente.risco;
      this.loadDataCliente();
      console.log(this.bairro)
    });

      this.mode = 'atualizar';

    }







  }

  loadDataCliente(){
    this.form = new FormGroup({
      bairro: new FormControl(this.bairro, [Validators.required]),
      cep: new FormControl(this.cep, [Validators.required]),
      nome: new FormControl(this.nome, [Validators.required]),
      numero: new FormControl(this.numero, [Validators.required]),
      renda: new FormControl(this.renda, [Validators.required]),
      rua: new FormControl(this.rua, [Validators.required]),
      risco: new FormControl(this.risco),
    });
  }

  retornarCamposRegistrar(){
    return this.form.value;
  }

  registrar(){

    const cliente = this.retornarCamposRegistrar();
    console.log(cliente);

    this.clienteService.salvar(cliente).subscribe(success =>{
      console.log('sucesso: ' + success);
      this.openSnackBar('Usuario Cadastrado com Sucesso', 'Ok');
    });

  }

  atualizar(){
    const cliente = this.retornarCamposRegistrar();
    console.log('atualizar:', cliente);

    this.clienteService.atualizar(this.idCliente, cliente).subscribe(success =>{
      console.log('sucesso: ' + success);
      this.openSnackBar('Usuario atualizado com Sucesso', 'Ok');
    });
  }

  backTo(){
    this.router.navigate(['']);
  }

  openSnackBar(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 4000,

    });
    this.backTo();
  }



}
