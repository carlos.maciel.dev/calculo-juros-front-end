import { SobreComponent } from './../../shared/sobre/sobre.component';
import { SimularEmprestimoComponent } from './../../shared/simular-emprestimo/simular-emprestimo.component';
import { ExcluirDialogComponent } from './../../shared/excluir-dialog/excluir-dialog.component';
import { Component, OnInit } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente-service.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { HistoricoSimulacaoComponent } from 'src/app/shared/historico-simulacao/historico-simulacao.component';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  constructor(private clienteService: ClienteService, private router: Router, public dialog: MatDialog) { }

  dataCliente: any;
  risco: any;
  ngOnInit() {
    this.loadData();



  }

  loadData() {
    this.clienteService.getAllClientes().subscribe(clientes =>{
      this.dataCliente = clientes;
      console.log(clientes);
    });
  }

  editar(idCliente){
    this.router.navigate(['cliente/' + idCliente]);
  }

  registrar(){
    this.router.navigate(['cliente/id']);
  }


  deletar(cliente): void {
     this.dialog.open(ExcluirDialogComponent, {
       data: {cliente}
    }).afterClosed().subscribe(result =>{
      this.loadData();
    });
  }

  simular(cliente){
    const dialogRef = this.dialog.open(SimularEmprestimoComponent,{

      data: {cliente}

    }).afterClosed().subscribe(result => {

    })
  }

  historico(cliente){
    const dialogRef = this.dialog.open(HistoricoSimulacaoComponent,{

      data: {cliente}

    }).afterClosed().subscribe(result => {

    });
  }

  irSobre() {
    this.dialog.open(SobreComponent,{
      width: '600px'
    }

    );
  }


}
