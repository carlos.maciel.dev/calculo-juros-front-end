import { SimulacaoModel } from './../model/simulacao.model';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SimulacaoService {
  API = environment.restApiServer;
  private url = `${this.API}/simulacao`;
  constructor(private http: HttpClient) { }


  salvar(id, simulacao: SimulacaoModel): Observable<SimulacaoModel>{
    return this.http.post<SimulacaoModel>(`${this.url}/${id}`, simulacao);
  }

  getHistorico(id): Observable<SimulacaoModel>{
    return this.http.get<SimulacaoModel>(`${this.url}/${id}/simulacoes`);
  }

}
