import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cliente } from '../model/cliente.model';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {


  API = environment.restApiServer;
  // API = 'http://192.168.0.106:8080';

  private url = `${this.API}/cliente`;

  constructor(private http: HttpClient) { }



  getAllClientes(): Observable<any[]> {
    return this.http.get<any[]>(`${this.url}`);
  }

  delete(id: number): Observable<any> {
    return this.http.delete<any>(`${this.url}/${id}`);
  }

  getId(id: number): Observable<any> {
    return this.http.get<any>(`${this.url}/${id}`);
  }


  salvar(cliente: Cliente): Observable<Cliente>{
    return this.http.post<Cliente>(`${this.url}`, cliente);
  }

  atualizar(id: number, cliente: Cliente): Observable<Cliente>{
    return this.http.put<Cliente>(`${this.url}/${id}`, cliente);
  }



}
