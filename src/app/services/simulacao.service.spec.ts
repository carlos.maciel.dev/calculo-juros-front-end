import { TestBed } from '@angular/core/testing';

import { SimulacaoService } from './simulacao.service';

describe('SimulacaoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SimulacaoService = TestBed.get(SimulacaoService);
    expect(service).toBeTruthy();
  });
});
