import { Component, OnInit, Inject } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente-service.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-excluir-dialog',
  templateUrl: './excluir-dialog.component.html',
  styleUrls: ['./excluir-dialog.component.css']
})
export class ExcluirDialogComponent implements OnInit {

  constructor(private clienteService: ClienteService,
    public dialogRef: MatDialogRef<ExcluirDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private snackbar:MatSnackBar) { }

  ngOnInit() {
    console.log(this.data);
  }

  onNoClick(){
    this.dialogRef.close();
  }

  onYesClick(){
    this.clienteService.delete(this.data.cliente.id).subscribe(excluir =>{
      console.log('sucesso', excluir);
      this.dialogRef.close();
      this.openSnackBar('Cliente Excluido com Sucesso', 'OK');
    })
  }

  openSnackBar(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 4000,

    });

  }




}
