import { Component, OnInit, Inject } from '@angular/core';
import { SimulacaoService } from 'src/app/services/simulacao.service';
import { SimularEmprestimoComponent } from '../simular-emprestimo/simular-emprestimo.component';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-historico-simulacao',
  templateUrl: './historico-simulacao.component.html',
  styleUrls: ['./historico-simulacao.component.css']
})
export class HistoricoSimulacaoComponent implements OnInit {
 displayedColumns: string[] = ['id', 'dataInicio', 'valorParcela', 'valorJuros', 'valorTotalJuros'];
  dataSource: any;
  constructor(private simulacaoService: SimulacaoService,
              public dialogRef: MatDialogRef<HistoricoSimulacaoComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private snackbar: MatSnackBar) { }

  ngOnInit() {

    this.simulacaoService.getHistorico(this.data.cliente.id).subscribe(historico =>{
      console.log(historico);
      this.dataSource = historico;
    })
  }



}
