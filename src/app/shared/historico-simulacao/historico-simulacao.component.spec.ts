import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricoSimulacaoComponent } from './historico-simulacao.component';

describe('HistoricoSimulacaoComponent', () => {
  let component: HistoricoSimulacaoComponent;
  let fixture: ComponentFixture<HistoricoSimulacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricoSimulacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricoSimulacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
