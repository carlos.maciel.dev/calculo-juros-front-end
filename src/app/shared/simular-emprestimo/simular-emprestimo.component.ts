import { Component, OnInit, Inject } from '@angular/core';
import { ClienteService } from 'src/app/services/cliente-service.service';
import { SimulacaoService } from 'src/app/services/simulacao.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Form, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-simular-emprestimo',
  templateUrl: './simular-emprestimo.component.html',
  styleUrls: ['./simular-emprestimo.component.css']
})
export class SimularEmprestimoComponent implements OnInit {
  formSimulacao: FormGroup;

  resp: any;
  datas: any;
  constructor(private simulacaoService: SimulacaoService,
              public dialogRef: MatDialogRef<SimularEmprestimoComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.resp = false;
    console.log(this.data);

    this.formSimulacao = new FormGroup({
      valorSolicitado: new FormControl( '', [Validators.required]),
      quantidadeParcelas: new FormControl(    '', [Validators.required]),
    });
  }

  onNoClick(){
    this.dialogRef.close();
  }

  checkValueForm() {
   return this.formSimulacao.value;
  }

  onYesClick() {

   const valueform = this.checkValueForm();
   this.simulacaoService.salvar(this.data.cliente.id, valueform).subscribe(resp =>{
      console.log('sucesso', resp);
      // this.dataSource = resp;
      this.datas = resp;
      this.resp = true;

    });
  }

  openSnackBar(message: string, action: string) {
    this.snackbar.open(message, action, {
      duration: 4000,

    });

  }
}

