import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClienteComponent } from './view/cliente/cliente.component';
import { CreateEditClienteComponent } from './view/cliente/create-edit-cliente/create-edit-cliente.component';

const routes: Routes = [
  { path: '', redirectTo: '/cliente', pathMatch: 'full' },
  { path: 'cliente', component: ClienteComponent },
  { path: 'cliente/:id', component: CreateEditClienteComponent },



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
